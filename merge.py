import os
import glob
import sys
import ROOT
from array import array

names=[#'508712_A400S0.4',
       #'506769_A500S0.4',
       #'508713_A600S0.4',
       #'508714_A700S0.4',
       #'506770_A800S0.4',
       #'508715_A900S0.4',
       '508716_H400S0.4',
       #'506771_H500S0.4',
       #'508717_H600S0.4',
       #'508718_H700S0.4',
       #'506772_H800S0.4',
       #'508719_H900S0.4',
       ]

for name in names:
   outputfile = os.path.join('/afs/cern.ch/work/c/caiyi/public/AHtt_NewWeights/',name+'_weight.root')
   output = ROOT.TFile(outputfile, "RECREATE")
   out_tree = ROOT.TTree("CollectionTree","CollectionTree")
   out_tree.SetAutoSave(0)
   eventNumber  = array('i',[0])
   mcChannelNumber  = array('i',[0])
   out_tree.Branch("eventNumber", eventNumber, "eventNumber/I")
   out_tree.Branch("mcChannelNumber", mcChannelNumber, "mcChannelNumber/I")
   tree_names = []
   weights = []
   inputs = []
   in_trees = []

   for file in glob.iglob(os.path.join('/eos/user/c/caiyi/2HDM/Run/*/EXOT',name+'*_weight.root')):
      print(file)
      tree_names.append("w_"+file.split("/")[-1].split('_')[3])
      weights.append(array('f',[0]))
      out_tree.Branch(tree_names[-1],weights[-1],tree_names[-1]+"/F")
      inputs.append(ROOT.TFile.Open(file,"READ"))
      in_trees.append(inputs[-1].Get("CollectionTree"))

   for i in xrange(in_trees[0].GetEntriesFast()):
      if i % 5000 == 0:
         print("*******"+str(i))
      for j in range(len(weights)):
         in_trees[j].GetEntry(i)
         weights[j][0] = getattr(in_trees[j],tree_names[j])
      eventNumber[0] = getattr(in_trees[-1],"eventNumber")
      mcChannelNumber[0] = getattr(in_trees[-1],"mcChannelNumber")
      out_tree.Fill()

   output.Write()
   output.Close()