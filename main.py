#!/usr/bin/env python2.7
# PYTHON_ARGCOMPLETE_OK
import config as c
from utils import get_mg5version
import Sim
import os
import Process as P

def set_config(**kwarg):
    for k, v in kwarg.items():
        setattr(c, k, v)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='MadgraphPythia Type-II 2HDM gg > H/A > tt~ (+ Interf) production.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    basepa = argparse.ArgumentParser(add_help=False)
    parser.add_argument("-i", "--iseed", type = int, default = c.iseed, help = "Random seed number")
    basepa.add_argument("-i", "--iseed", type = int, default = c.iseed, help = "Random seed number")
    parser.add_argument("-e", "--ecm", type = int, help = "Central Mass Energy sqrt(s).")
    basepa.add_argument("-e", "--ecm", type = int, help = "Central Mass Energy sqrt(s).", required = True)
    parser.add_argument("--param_card", default = None, help = "Instead of directly using the parameter set from run number/parameters, import a MG5 parameter card. Only used when generating event")
    basepa.add_argument("--param_card", default = None, help = "Instead of directly using the parameter set from run number/parameters, import a MG5 parameter card. Only used when generating event")
    parser.add_argument("-p", "--parton_level", nargs = '*', metavar = 'sim_opt', help = "Parton-level simulation. Powered by MG5_aMC@NLO{version}".format(version = 'v' + '.'.join(map(str, get_mg5version(c.MG5_path)))))
    basepa.add_argument("-p", "--parton_level", nargs = '*', metavar = 'sim_opt', help = "Parton-level simulation. Powered by MG5_aMC@NLO{version}".format(version = 'v' + '.'.join(map(str, get_mg5version(c.MG5_path)))))
    parser.add_argument("-pv", "--parton_level_validation", nargs = '*', metavar = 'sim_opt', help = "Produce a ntuple that has all the neccessary parton-level information inside. The Matrix Element will be calculated as well. If <FFMT> is given, then the file matching will be done and stored into the ntuples.")
    basepa.add_argument("-pv", "--parton_level_validation", nargs = '*', metavar = 'sim_opt', help = "Produce a ntuple that has all the neccessary parton-level information inside. The Matrix Element will be calculated as well. If <FFMT> is given, then the file matching will be done and stored into the ntuples.")
    parser.add_argument("-t", "--truth_level", nargs = '*', metavar = 'sim_opt', help = "Truth-level simulation. Powered by Pythia6")
    basepa.add_argument("-t", "--truth_level", nargs = '*', metavar = 'sim_opt', help = "Truth-level simulation. Powered by Pythia6")
    parser.add_argument("-tv", "--truth_level_validation", nargs = '*', metavar = 'sim_opt', help = "Produce a ntuple that has all the neccessary truth-level information. RWGT will be done at the procedure.")
    basepa.add_argument("-tv", "--truth_level_validation", nargs = '*', metavar = 'sim_opt', help = "Produce a ntuple that has all the neccessary truth-level information. RWGT will be done at the procedure.")
    parser.add_argument("-nv", "--new_level_validation", nargs = '*', metavar = 'sim_opt', help = "New reweight.")
    basepa.add_argument("-nv", "--new_level_validation", nargs = '*', metavar = 'sim_opt', help = "New reweight.")
    parser.add_argument("-s", "--systematics", nargs = '*', help = 'Caculate systematics uncertainty.')
    basepa.add_argument("-s", "--systematics", nargs = '*', help = 'Caculate systematics uncertainty.')
    parser.add_argument("-o", "--official", nargs = '*', help = 'Use official samples.')
    basepa.add_argument("-o", "--official", nargs = '*', help = 'Use official samples.')
    parser.add_argument("-c", "--cluster", default = "True", help = "Batch system to use.")
    basepa.add_argument("-c", "--cluster", default = "True", help = "Batch system to use.")
    parser.add_argument("--ffmt", type = str, default = "DAOD_TRUTH1", help = "File format that will be produced or matched")
    basepa.add_argument("--ffmt", type = str, default = "DAOD_TRUTH1", help = "File format that will be produced or matched")
    parser.add_argument("--model", type = str, choices = ['2HDM', '2HDM+a','CMS'], help = 'Model to run.')
    basepa.add_argument("--model", type = str, choices = ['2HDM', '2HDM+a','CMS'], help = 'Model to run.', required = True)
    parser.add_argument("--gen_mode", type = str, choices = ['MG', 'MS'], default = 'MS', help = 'Module to generate.')
    basepa.add_argument("--gen_mode", type = str, choices = ['MG', 'MS'], default = 'MS', help = 'Module to generate.')
    parser.add_argument("--reweight", nargs = '*', help = 'Reweighting framework.')
    basepa.add_argument("--reweight", nargs = '*', help = 'Reweighting framework.')
    parser.add_argument("--rw_mediator", default = None, help = 'Reweighting parameters')
    basepa.add_argument("--rw_mediator", default = None, help = 'Reweighting parameters')
    parser.add_argument("--rw_mass", default = None, help = 'Reweighting parameters')
    basepa.add_argument("--rw_mass", default = None, help = 'Reweighting parameters')
    parser.add_argument("--rw_tanb", default = None, help = 'Reweighting parameters')
    basepa.add_argument("--rw_tanb", default = None, help = 'Reweighting parameters')
    parser.add_argument("--rw_signal_type", default = None, help = 'Reweighting parameters')
    basepa.add_argument("--rw_signal_type", default = None, help = 'Reweighting parameters')
    subparsers = parser.add_subparsers()
    # Specify a process using physical parameters.
    parser_parameters = subparsers.add_parser("parameters",
                                              parents = [basepa],
                                              description="Specify a process using physical parameters.",
                                              help="Specify a process using physical parameters.",
                                              formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_parameters.add_argument("mediator", help = "A possible Type-II 2HDM Mediator.")
    parser_parameters.add_argument("mass", help = "Mass of the type-II 2HDM Mediator.")
    parser_parameters.add_argument("tanb", help = "The ratio of the two vacuum expectation values.")
    parser_parameters.add_argument("signal_type", choices = ["S", "SI" ,"B","TB","I"], help = "Signal Only / Signal + Interf / Signal + Interf + Bakground / Background / Interf Only")
    parser_parameters.add_argument("slicer", default = "[:1000000:10000]", type = Sim.StrIterator, nargs = "?", help = "[start:stop:step_size]")
    parser_parameters.add_argument("ma", default = 100.0, help = "Mass of a./gH0tt")
    parser_parameters.add_argument("sinp", default = 0.0, help = "Parameter in 2HDM+a./gA0tt")
    
    args, additional_args = basepa.parse_known_args(namespace = parser.parse_args())

    if args.reweight is not None:
        rw_proc = P.ttbarResonanceProcess
        rw_process = rw_proc(mediator = args.rw_mediator or args.mediator, mass = float(args.rw_mass or args.mass), tanb = float(args.rw_tanb or "0.4"), signal_type = args.rw_signal_type or "S")
    else:
        rw_process = None

    if args.model == '2HDM':
        proc = P.ttbarResonanceProcess
        process = proc(mediator = args.mediator, mass = float(args.mass), tanb = float(args.tanb), signal_type = args.signal_type, reweighted_from = rw_process)
    elif args.model == '2HDM+a':
        proc = P.THDMaTtRes_FormFac
        process = proc(mediator = args.mediator, mass = float(args.mass), tanb = float(args.tanb), ma = float(args.ma), sinp = float(args.sinp), signal_type = args.signal_type, reweighted_from = rw_process)
    elif args.model == 'CMS':
        proc = P.CMS_test
        process = proc(mediator = args.mediator, mass = float(args.mass), tanb = float(args.tanb), gH0tt = float(args.ma), gA0tt = float(args.sinp), signal_type = args.signal_type, reweighted_from = rw_process)

    sim_chain = []
    systematics = True if (args.systematics is not None) else False
    official = True if (args.official is not None) else False
    use_cluster = False if args.cluster=="False" else True
    if args.slicer == None:
        use_slicer = False
        args.slicer = c.slicer
    else:
        use_slicer = True

    if args.param_card == "auto" or official:
        card_ = os.path.join(c.root_path,"param_card",args.model+"_official",args.mediator+args.mass+".dat")
        param_ = card_ if (os.path.exists(card_) and float(args.tanb)==0.4) else None
    else:
        param_ = args.param_card

    kwargs = dict(process = process, slicer = args.slicer, iseed = args.iseed, ebeam1 = args.ecm/2, ebeam2 = args.ecm/2, use_cluster = False if args.cluster=="False" else True, systematics = systematics, gen_mode = args.gen_mode, official = official)
    if param_ != None:
        kwargs['param_card']=param_

    if args.parton_level is not None:
        s = Sim.PartonLevelSim(**kwargs)
        for p in args.parton_level:
            exec('s.{}'.format(p))
        sim_chain.append(s.run)

    if args.truth_level is not None:
        s = Sim.TruthLevelSim(**kwargs)
        for p in args.truth_level:
            exec('s.{}'.format(p))
        sim_chain.append(s.run)

    if args.parton_level_validation is not None:
        if args.parton_level is None:
            s = Sim.PartonLevelSim(**kwargs)
        for p in args.parton_level_validation:
            exec('s.{}'.format(p))
        sim_chain.append(s.validate)

    if args.truth_level_validation is not None:
        if args.truth_level is None:
            s = Sim.TruthLevelSim(**kwargs)
        for p in args.truth_level_validation:
            exec('s.{}'.format(p))
        sim_chain.append(s.validate)

    if args.new_level_validation is not None:
        s = Sim.NewLevelSim(**kwargs)
        for p in args.new_level_validation:
            exec('s.{}'.format(p))
        sim_chain.append(s.validate)
        
    for sim in sim_chain:
        sim(use_slicer, match = 'EXOT' if official else args.ffmt)
