import Process as P
import Sim
Sims = []
for mediator in ['A']:#['A', 'H']:
    for tanb in [0.4, 1, 2, 5, 9][:1]:
        for ma in [100]:#,200,300,400,500]:
            for sinp in [0,0.35,2**(-0.5)][-1:]:#,1]:
                p=P.THDMaTtRes(mass=600,mediator=mediator,ma=ma,tanb=tanb,MXd=10.,sinp=sinp,signal_type='SI')
                s=Sim.PartonLevelSim(p,decay=False)
                s.ebeam1=4000
                s.ebeam2=4000
                s.slicer="[0:250000:10000]"
                s.dynamical_scale_choice=3 
                s.output_path = "{{ecm}}TeV.{mediator}{{process.mass}}{tanb:03}1".format(mediator=mediator, tanb=int(p.tanb*100),sinp=int(p.sinp*100), MXd=int(p.MXd),p=p)                                                                                                                        
                # s.output_path = "{{ecm}}TeV.{mediator}{{process.mass}}{tanb:03}0_sinp{sinp:03}_ma{p.ma:03}_mxd{MXd:03}".format(mediator=mediator, tanb=int(p.tanb*100),sinp=int(p.sinp*100), MXd=int(p.MXd),p=p)
                # raise StopIteration
                if s.initialise() != 'DONE':
                    s.execute()
                print p.Wh1, p.Wh2, p.Wh3, p.Wh4
                Sims.append(s)
                # s_rw = Sim.BasicRW(p, ebeam1 = s.ebeam1, ebeam2 = s.ebeam2)
                # s_rw.output_path = s.output_path
                # raise StopIteration

for s in Sims:
    s.validate(matrix_element = False, match = "")