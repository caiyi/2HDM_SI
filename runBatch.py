from datetime import date
from subprocess import call
import argparse
import os
cwd = os.getcwd()
fp = "/afs/cern.ch/work/c/caiyi/condor/"

#parser = argparse.ArgumentParser()
#parser.add_argument('-n', '--run_number', type=str)
#parser.add_argument('-e', '--ecm', type=str)
#parser.add_argument('-m', '--model', type=str)
#result = parser.parse_args()

ah = 'H'
m = '400'
tanb = '0.4'
si = 'S'
Event_slice = '\"[:800000:800000]\"'
ecm = '13000'
model = '2HDM'

def makecommand():
    commands = []
    runstr = cwd + "/main.py parameters {:s} {:s} {:s} {:s} {:s} 100 0 --model {:s} -e {:s} -pv\n".format(ah, m, tanb, si, Event_slice, model, ecm)
    commands.append(runstr)
    return commands
commands = makecommand()

execstr = ""
execstr += model+'_'+ah+m+si+tanb+'_'+ecm

tmp = """#!/bin/sh
function setupATLAS() {
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
}
setupATLAS"""+"""
cd {:s}

echo Now set up environment
source {:s}/setup.sh

echo Now running
""".format(cwd,cwd)

submitstr = """#!/bin/tcsh
#
executable              = %s.sh
arguments               = $(ClusterId)$(ProcId)
output                  = %s.$(ClusterId).$(ProcId).out
error                   = %s.$(ClusterId).$(ProcId).err
log                     = %s.$(ClusterId).log
+JobFlavour             = "testmatch"
RequestCpus             = 2
queue
"""

bsubscript = tmp+'\n'
for com in commands:
    bsubscript=bsubscript+com
submitbsub = submitstr % (execstr+'_cmd',execstr,execstr,execstr)
exc = open(fp+execstr+'_exc.sub', 'w')
exc.write(submitbsub)
exc.close()
exc = open(fp+execstr+'_cmd.sh', 'w')
exc.write(bsubscript)
exc.close()
call(['chmod', '744', fp+execstr+'_exc.sub'])
call(['chmod', '744', fp+execstr+'_cmd.sh'])

excscript = open(fp+'runBatch.sh', 'w')
excscript.write('cd '+fp+'\n')
excscript.write('condor_submit '+execstr+'_exc.sub\n')
excscript.write('cd '+cwd+'\n')
excscript.close()

print(fp+execstr+'_cmd.sh')
call(['bash', os.path.join(fp,'runBatch.sh')])
