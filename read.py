import utils
import ROOT
import os

utils.rcSetup(os.path.join("Core", "AnalysisTop"))
chain = ROOT.TChain("CollectionTree")
for file in os.listdir("/afs/cern.ch/user/c/caiyi/eos/2HDM/Run/13TeV.H400.00400_/EXOT"):
    chain.Add(os.path.join("/afs/cern.ch/user/c/caiyi/eos/2HDM/Run/13TeV.H400.00400_/EXOT",file))
tree = ROOT.xAOD.MakeTransientTree(chain)
for i in xrange(tree.GetEntriesFast()):
    if i%10000==0: print(i)
    tree.GetEntry(i)
    if tree.EventInfo.eventNumber()>801000 and tree.EventInfo.eventNumber()<802000:
        print(tree.EventInfo.eventNumber())