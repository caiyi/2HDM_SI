#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
export SITE_NAME=DESY-HH

cd {work_path}

exit_when_fail() {{
	rc=$?
	valid_rc="$@"
	for v in $( echo "$valid_rc" )
	do
		if [[ $rc == $v ]]
		then
			if [[ $rc != 0 ]]; then echo "WARNING: user-allowed non-zero return code: $rc"; fi
		 	return 0
		fi
	done
	echo "Exit on RC: $rc"
	unset valid_rc
	exit $rc
}}

if [ '{evgenJobOpts}' = '' ]; then 
	ln -s {jo} .
	ln -s {Athena}/MC15JobOptions .
fi

asetup 21.6.63,AthGeneration
if [ ! -f {outputEVNTFile} ]; then
	echo "Showering start..."
	Gen_tf.py --randomSeed=2001 --firstEvent={firstEvent} --jobConfig=./ --outputEVNTFile={outputEVNTFile} --inputGeneratorFile={inputGeneratorFile} {evgenJobOpts}
	exit_when_fail 0 65
	echo "Showering succesfully finished."
fi

asetup 21.2,AthDerivation,latest
if [ '{fmt[0]}' = 'DAOD' ]; then
	touch {outputFile}
	echo "EVNT->DAOD conversion Start..."
	Reco_tf.py --inputEVNTFile={outputEVNTFile} --outputDAODFile=tmp.root --maxEvents=-1 --reductionConf={fmt[1]}
	exit_when_fail 0
	rsync -a {fmt[0]}_{fmt[1]}.tmp.root {outputFile} --remove-source-files
	echo "EVNT->DAOD conversion succesfully finished."
elif [ '{fmt[0]}' = 'NTUP' ]; then
	Reco_tf.py --inputEVNTFile={outputEVNTFile} --outputNTUP_TRUTHFile={outputFile} --maxEvents=-1 --preExec='from D3PDMakerConfig.D3PDMakerFlags import jobproperties;jobproperties.D3PDMakerFlags.TruthWriteEverything=True'
	exit_when_fail 0
fi

cd {root_path}
rm -rf {work_path}

rm {outputEVNTFile}
