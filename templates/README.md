BKG Subtraction
---------------
1. `<rootpath>/core/MG5_aMC_v2_x_y/madgraph/iolibs/template_files/matrix_madevent_group_v4.inc`, which response for the ME calculation in `MadEvent`, will be replaced by either the modified version: `matrix_madevent_group_v4.inc.SI` or the original version: `matrix_madevent_group_v4.inc` depending on if you need BKG subtraction or not.
2. A partial diff is given below.

	```diff
	diff --git a/templates/matrix_madevent_group_v4.inc b/templates/matrix_madevent_group_v4.inc.SI
	index c441a3c..e02d3fe 100644
	--- a/templates/matrix_madevent_group_v4.inc
	+++ b/templates/matrix_madevent_group_v4.inc.SI
	@@ -254,6 +254,7 @@ C
	 C   Needed for v4 models
	     COMPLEX*16 DUM0,DUM1
	     DATA DUM0, DUM1/(0d0, 0d0), (1d0, 0d0)/
	+    REAL*8 MATRIX_BKG
	 C
	 C FUNCTION
	 C
	@@ -289,6 +290,23 @@ C ----------
	       ENDDO
	        ENDDO
	 
	+
	+    MATRIX_BKG = 0.D0
	+  DO M = 1, NAMPSO
	+      DO I = 1, 2
	+        ZTEMP = (0.D0,0.D0)
	+        DO J = 1, 2
	+          ZTEMP = ZTEMP + CF(J,I)*JAMP(J,M)
	+        ENDDO
	+    DO N = 1, NAMPSO
	+          IF (CHOSEN_SO_CONFIGS(SQSOINDEX%(proc_id)s(M,N))) THEN
	+         MATRIX_BKG = MATRIX_BKG + ZTEMP*DCONJG(JAMP(I,N))/DENOM(I)
	+      ENDIF
	+    ENDDO
	+      ENDDO
	+  ENDDO
	+      MATRIX%(proc_id)s = MATRIX%(proc_id)s - MATRIX_BKG
	+
	 %(amp2_lines)s
	     Do I = 1, NCOLOR
	          DO M = 1, NAMPSO      
	```