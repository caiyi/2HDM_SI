# Using globaly provided gcc from /cvmfs/sft.cern.ch/lcg/contrib
# found package blas-bb5ea
# found package Python-e553a
# found package sqlite-472f2
# found package lapack-bb169
# found package blas-bb5ea
# found package setuptools-43c0e
# found package Python-e553a
source /cvmfs/sft.cern.ch/lcg/contrib/gcc/8/x86_64-centos7/setup.sh;
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/numpy/1.16.4/x86_64-centos7-gcc8-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/numpy/1.16.4/x86_64-centos7-gcc8-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/numpy/1.16.4/x86_64-centos7-gcc8-opt/lib/python2.7/site-packages:$PYTHONPATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/blas/0.3.5.openblas/x86_64-centos7-gcc8-opt/lib64:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/blas/0.3.5.openblas/x86_64-centos7-gcc8-opt/lib64/pkgconfig:$PKG_CONFIG_PATH";
export BLAS__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/blas/0.3.5.openblas/x86_64-centos7-gcc8-opt";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/Python/2.7.16/x86_64-centos7-gcc8-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/Python/2.7.16/x86_64-centos7-gcc8-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/Python/2.7.16/x86_64-centos7-gcc8-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/Python/2.7.16/x86_64-centos7-gcc8-opt/lib/python2.7/site-packages:$PYTHONPATH";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/sqlite/3280000/x86_64-centos7-gcc8-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/sqlite/3280000/x86_64-centos7-gcc8-opt/lib:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/sqlite/3280000/x86_64-centos7-gcc8-opt/lib/pkgconfig:$PKG_CONFIG_PATH";
export SQLITE__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/sqlite/3280000/x86_64-centos7-gcc8-opt";
export PYTHON__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/Python/2.7.16/x86_64-centos7-gcc8-opt";
cd "/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/Python/2.7.16/x86_64-centos7-gcc8-opt"
export PYTHONHOME="${PYTHON__HOME}"
export PYTHONVERSION=`python -c "from __future__ import print_function; import sys; print ('%d.%d' % (sys.version_info.major, sys.version_info.minor))"`
cd - 1>/dev/null # from /cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/Python/2.7.16/x86_64-centos7-gcc8-opt
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/lapack/3.8.0/x86_64-centos7-gcc8-opt/lib64:$LD_LIBRARY_PATH";
export PKG_CONFIG_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/lapack/3.8.0/x86_64-centos7-gcc8-opt/lib64/pkgconfig:$PKG_CONFIG_PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/lapack/3.8.0/x86_64-centos7-gcc8-opt/lib64/cmake:$LD_LIBRARY_PATH";
export LAPACK__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/lapack/3.8.0/x86_64-centos7-gcc8-opt";
export PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/setuptools/41.0.0/x86_64-centos7-gcc8-opt/bin:$PATH";
export LD_LIBRARY_PATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/setuptools/41.0.0/x86_64-centos7-gcc8-opt/lib:$LD_LIBRARY_PATH";
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/setuptools/41.0.0/x86_64-centos7-gcc8-opt/lib/python2.7/site-packages:$PYTHONPATH";
export SETUPTOOLS__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/setuptools/41.0.0/x86_64-centos7-gcc8-opt";
export NUMPY__HOME="/cvmfs/sft.cern.ch/lcg/releases/LCG_97_ATLAS_1/numpy/1.16.4/x86_64-centos7-gcc8-opt";
export PATH=$HOME/.local/bin:$PATH
