#!/usr/bin/env python
import os,re,glob,sys
import utils
import ROOT as root
import numpy as np
'''
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as aplt
import tarfile
'''

ID_A = [506769,506770,508712,508713,508714,508715]
fold_A = ["13TeV.A500.00400_","13TeV.A800.00400_","13TeV.A400.00400_","13TeV.A600.00400_","13TeV.A700.00400_","13TeV.A900.00400_"]
ID_H = [506771,506772,508716,508717,508718,508719]
fold_H = ["13TeV.H500.00400_","13TeV.H800.00400_","13TeV.H400.00400_","13TeV.H600.00400_","13TeV.H700.00400_","13TeV.H900.00400_"]
input = sys.argv[1]
i_va = int(sys.argv[2])
if input=='A':
    ID = ID_A
    fold = fold_A
elif input=='H':
    ID = ID_H
    fold = fold_H
hist = root.TH1D("500GeV_"+input,"500GeV_"+input,60,300,900)

utils.rcSetup(os.path.join("Core","AnalysisTop"))

if i_va==0:
    chain_SI = root.TChain("CollectionTree")
    for file in glob.iglob("/afs/cern.ch/work/c/caiyi/SI_EXOT/{}/*".format(input)):
        chain_SI.Add(file)
    tree_SI = root.xAOD.MakeTransientTree(chain_SI)
    for i in xrange(tree_SI.GetEntriesFast()):
        if i%10000==0:
            print("event#######"+str(i))
        tree_SI.GetEntry(i)
        is_t = False
        is_tx = False
        for top in tree_SI.TruthParticles:
            if (top.pdgId() == 6) and not(is_t):
                is_t = True
                t = top.p4()
            elif (top.pdgId() == -6) and not(is_tx):
                is_tx = True
                tx = top.p4()
            if is_t and is_tx:
                break
        hist.Fill((t+tx).M()/1000,tree_SI.EventInfo.mcEventWeight(0))

else:
    chain = root.TChain("CollectionTree")
    for file in glob.iglob("/eos/user/c/caiyi/2HDM/Run/{}/EXOT/*".format(fold[i_va-1])):
        chain.Add(file)
    tree = root.xAOD.MakeTransientTree(chain)
    for file in glob.iglob("/afs/cern.ch/work/c/caiyi/public/AHtt_NewWeights/{}*".format(ID[i_va-1])):
        root_file = root.TFile.Open(file,"READ")
    br = root_file.Get("CollectionTree")

    for i in xrange(10000):
        if i%10000==0:
            print("event#######"+str(i))
        tree.GetEntry(i)
        br.GetEntry(i)
        is_t = False
        is_tx = False
        for top in tree.TruthParticles:
            if (top.pdgId() == 6) and not(is_t):
                is_t = True
                t = top.p4()
            elif (top.pdgId() == -6) and not(is_tx):
                is_tx = True
                tx = top.p4()
            if is_t and is_tx:
                break
        hist.Fill((t+tx).M()/1000,getattr(br,"w_{}500SI0.4".format(input)))

output = root.TFile("/afs/cern.ch/work/c/caiyi/public/rw_plot/rw_{}_{}_10000.root".format(input,i_va),'RECREATE')
hist.Write()
output.Close()