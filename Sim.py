import os
import sys
import shutil
import Process
import subprocess
import glob
import tarfile
from collections import OrderedDict
import utils
import config as c
import Core.EventMatching.Events
from Core.EventMatching.TreeReader import TreeReader as tr
tr.get_logger().level = 20
import madgraph.various.lhe_parser as lhe_parser
import imp
import tempfile
import re
import logging
from array import array

logger = c.getLogger("2HDM_ttbarSI.Sim")

def get_fct(logger = None):
    if logger is None:
        def fct(idle, run, finish):
            l = "Idle: {:3},  Running: {:3},  Completed: {:3}".format(idle, run, finish)
            print l
    else:
        def fct(idle, run, finish):
            l = "Idle: {:3},  Running: {:3},  Completed: {:3}".format(idle, run, finish)
            logger.info(l)
    return fct

# some helper functions
def get_decay_chain(particle):
    """Get decay chain of a __TruthParticle__
    
    Get all the children of a __TruthParticle__ including itself.
    
    Parameters
    ----------
    particle : {TruthParticle}
        A __TruthParticle__ instance.
    
    Yields
    ------
    children : {TruthParticle}
        The particle itself and all of its children.
    }
    """
    yield particle
    for i in xrange(particle.nChildren()):
        for child in get_decay_chain(particle.child(i)):
            yield child

def category(p, prefix):
    pdgId = p.pdgId()
    anti = "x" if pdgId < 0 else ""
    if p.isW() or p.isZ():
        return prefix + "W"
    if pdgId in (6, -6):
        return "t" + anti
    if (pdgId in (5, -5)) and p.parent().isTop():
        return prefix + "b"
    return prefix + "j" + anti

def category_lhe(p, prefix):
    pdgId = p.pid
    anti = "x" if pdgId < 0 else ""
    if p.pid in (23, 24, -23, -24):
        return prefix + "W"
    if pdgId in (6, -6):
        return "t" + anti
    if (pdgId in (5, -5)) and p.mother1.pid in (6, -6):
        return prefix + "b"
    return prefix + "j" + anti    

def short_name(file_path):
    file_path = os.path.basename(file_path).split(".")
    return ".".join([file_path[3], filter(lambda s: s.startswith("_"), file_path)[0]])

def get_matching_events(flist, ffmt, callables = ["pdfInfo"]):
    ev_gen = (tr(f, ffmt, callables)() for f in flist)
    events = next(ev_gen)
    for _events in ev_gen:
        events.extend(_events)
    return events

def search_files(path,word):
    input = []
    for filename in os.listdir(path):
        fp = os.path.join(path,filename)
        if os.path.isfile(fp) and word in filename:
            input.append(fp)
    return input

def matching(f1list, f2list, ffmt1, ffmt2, callables1 = ["pdfInfo"], callables2 = ["pdfInfo"]):
    t1 = get_matching_events(f1list, ffmt1, callables1)
    t2 = get_matching_events(f2list, ffmt2, callables2)
    try:
        t1.match(t2)
    except RuntimeWarning:
        raise RuntimeError("Some Events find no match or multiple matches. Please check!")
    for ev in t2:
        i=len(ev.matches)
        if len(ev.matches)>1:
            i=0
            for ev_match in ev.matches:
                i=i+1
                print(ev_match.no)
                print(ev.eventNumber)
                if (int(ev_match.t.Px()/0.01)==int(ev.tPx/0.01)):
                    print("Success")
                    break
        #yield [len(ev.matches), ev.matches[len(ev.matches)-1].no if len(ev.matches) else 0, ev.no if len(ev.matches) else 0, ev.matches[len(ev.matches)-1].t if len(ev.matches) else 0, ev.matches[len(ev.matches)-1].tx if len(ev.matches) else 0, ev.matches[len(ev.matches)-1].g1 if len(ev.matches) else 0, ev.matches[len(ev.matches)-1].g2 if len(ev.matches) else 0, ev.matches[len(ev.matches)-1].aS if len(ev.matches) else 0]
        yield [i, ev.matches[i-1].no if len(ev.matches[i-1].no)==1 else [ev.eventNumber], ev.no if i else 0, ev.matches[i-1].t if i else 0, ev.matches[i-1].tx if i else 0, ev.matches[i-1].g1 if i else 0, ev.matches[i-1].g2 if i else 0, ev.matches[i-1].w1 if i else 0, ev.matches[i-1].w2 if i else 0, ev.matches[i-1].b if i else 0, ev.matches[i-1].bx if i else 0, ev.matches[i-1].aS if i else 0]

def check_rc():
    if "ROOTCOREBIN" not in os.environ:
        logger.warn(ImportWarning("Rootcore not yet set. Processing without Rootcore configured. Accessing DAOD files is not possible in this mode."))

if utils.get_mg5version(c.MG5_path) >= (2,6,0):
    def get_me(matrix_module):
        return matrix_module.get_value
    def initialisemodel(matrix_module):
        return matrix_module.initialisemodel
else:
    def get_me(matrix_module):
        def _get_me(*args):
            return matrix_module.get_me(*args)[0]
        return _get_me
    def initialisemodel(matrix_module):
        return matrix_module.initialise

class StrIterator(object):
    def __init__(self, a_str):
        t = type(a_str)
        if t is int:
            s = slice(a_str)
        elif t is str:
            s = slice(*(int(i) if i.strip() else None for i in a_str.lstrip("[").rstrip("]").split(":")))
        self.start, self.stop, self.step = s.start or 0, (s.stop or sys.maxint), s.step or 10000
    def __getitem__(self, y):
        return self._xrange.__getitem__(y)
    def __iter__(self):
        return iter(self._xrange)
    def __len__(self):
        return len(self._xrange)
    def __str__(self):
        return "[{}({}):{}({}):{}]".format(self.start, self.start_index, self.stop, self.stop_index, self.step)
    def __repr__(self):
        return str(self)
    @property
    def start_index(self):
        return self.start // self.step
    @property
    def stop_index(self):
        return (((self.stop - self.start)//self.step) * self.step + self.start) // self.step
    @property
    def _xrange(self):
        return xrange(self.start, self.stop_index * self.step, self.step)
    def piecewise(self):
        for i in range(self.start_index, self.stop_index):
            yield StrIterator("[{start}:{stop}:{step}]".format(start = i * self.step, stop = (i+1) * self.step, step = self.step))
    def get_cmd_repr(self):
        return "[{start}:{stop}:{step}]".format(start = self.start, stop = self.stop, step = self.step)

# Simulation Classes

class BasicSim(object):
    def __init__(self, process, slicer = "[:1000000:10000]", ebeam1 = 6500, ebeam2 = 6500, generator = None, dataset = {}, dataset_ext = None, dirname = "", **kwarg):
        if type(process) in (int, str):
            process = Process.ttbarResonanceProcess.from_run_number(process)
        self.process = process
        self.generator = generator
        self.dirname = dirname
        self.dataset_ext = dataset_ext
        self.slicer = slicer
        self.official = kwarg.get("official")
        self._ebeam1 = ebeam1
        self._ebeam2 = ebeam2
        self.dataset = dataset
        self._run_path_fmt = "{ecm}TeV.{process}"
        self._lhebanner = {}
        self._output_path = None
        self.use_slicer = kwarg.pop("use_slicer", None)
        self.decay = kwarg.get("decay", True)
        self.bkg_removal = kwarg.get('bkg_removal', process.sim_config.get('bkg_removal', True))
        self.use_cluster = kwarg.pop('use_cluster', False)
	import Core.clusters
        self.cluster = Core.clusters.from_name[c.cluster_type](cluster_type = None, cluster_status_update=(600,30), cluster_queue='None')
        self.cluster.job_flavour = 'workday'
    @property
    def nevents(self):
        return self.slicer.stop - self.slicer.start
    @property
    def split_size(self):
        return self.slicer.step
    @property
    def run_path(self):
        if not hasattr(self, "_run_path"):
            self.run_path = None
        return self._run_path
    @run_path.setter
    def run_path(self, run_path):
        if run_path is not None:
            self._run_path_fmt = run_path
        self._run_path = os.path.realpath(os.path.join(c.run_root, self._run_path_fmt.format(ecm = (self.ebeam1 + self.ebeam2)/1000, process = self.process)))
    @property
    def output_path(self):
        if self._output_path is None:
            self._output_path = os.path.join(self.run_path, self.dirname)
        return self._output_path
    @output_path.setter
    def output_path(self, output_path):
        self._output_path = output_path
    @property
    def ebeam1(self):
        return self._ebeam1
    @ebeam1.setter
    def ebeam1(self, ebeam1):
        self._ebeam1 = ebeam1
        self.run_path = None
    @property
    def ebeam2(self):
        return self._ebeam2
    @property
    def work_path(self):
        return os.path.join(self.run_path, "Work", self.dirname)
    @property
    def log_path(self):
        return os.path.join(self.run_path, "LOG", self.dirname)
    def initialise(self):
        pass
    def execute(self):
        pass
    def eventloop_1(self, i):
        pass
    def finalize(self):
        pass
    def validate(self):
        pass
    def check_output(self):
        dic = self.output_path
        if not os.path.exists(dic):
            os.makedirs(dic)
        if not os.path.exists(self.work_path):
            os.makedirs(self.work_path)
        if not os.path.exists(self.log_path):
            os.makedirs(self.log_path)
    def check_dataset(self, dirname = None, dataset_ext = None, use_slicer = None, inplace = False, use_fname = False, verbose = True, search = False):
        use_slicer = use_slicer or self.use_slicer
        dataset_ext = dataset_ext or self.dataset_ext
        dirname = dirname or self.dirname
        dataset = OrderedDict()
        if search:
            for ext in dataset_ext:
                for fname in search_files(path = os.path.join(self.run_path, dirname), word = ext):
                    dname = fname.split('/')[-1].strip(ext)
                    exists = os.path.exists(fname)
                    logger.info("Looking for {} [{}]".format(os.path.relpath(fname), 'O'))
                    if use_fname:
                        dataset.setdefault(fname, exists)
                    else:
                        dataset.setdefault(dname, OrderedDict()).update({ext: os.path.exists(fname)})
        else:
            if use_slicer:
                for ipart in xrange(self.slicer.start_index + 1, self.slicer.stop_index + 1):
                    for ext in dataset_ext:
                        dname = "{}._{:05d}".format(self.process, ipart)
                        fname = os.path.join(self.run_path, dirname, "{}.{}".format(dname, ext))
                        exists = os.path.exists(fname)
                        logger.info("Looking for {} [{}]".format(os.path.relpath(fname), 'O' if exists else 'X'))
                        if use_fname:
                            dataset.setdefault(fname, exists)
                        else:
                            dataset.setdefault(dname, OrderedDict()).update({ext: os.path.exists(fname)})
            else:
                exists = True
                for ext in dataset_ext:
                    pattern = os.path.join(self.run_path, dirname, "{}*.{}*".format(str(self.process), ext))
                    logger.info("Search file pattern: {}".format(os.path.relpath(pattern)))
                    for fname in sorted(glob.iglob(pattern)):
                        logger.info("Found {} [O]".format(fname))
                        if use_fname:
                            dataset.setdefault(fname, exists)
                        else:
                            dataset.setdefault(os.path.basename(fname).split("."+ext)[0], OrderedDict()).update({ext: exists})
        if inplace:
            self.dataset = dataset
        return dataset
    def remove_dataset(self, dirname = None, dataset_ext = None, use_slicer = None, verbose = True, dry_run = False):
        use_slicer = use_slicer or self.use_slicer
        dataset_ext = dataset_ext or self.dataset_ext
        dirname = dirname or self.dirname
        dataset = [f for f, exists in self.check_dataset(dirname = dirname, dataset_ext = dataset_ext, use_slicer = use_slicer, verbose=False, use_fname = True).iteritems() if exists]
        if verbose or dry_run:
            for d in dataset:
                logger.critical('>>> ' + os.path.relpath(d))
            logger.critical('The samples above are going to be removed. Are you sure? [y/N]')
            if not dry_run:
                if raw_input().strip().lower() == 'y':
                    for d in dataset: os.remove(d)
        else:
            for d in dataset: os.remove(d)
        logger.critical('DONE!')
        

    @property
    def slicer(self):
        return self._slicer
    @slicer.setter
    def slicer(self, a_str):
        if isinstance(a_str, StrIterator):
            self._slicer = a_str
        else:
            self._slicer = StrIterator(a_str)
    def run(self, *arg, **kwarg):
        if self.initialise() == "DONE":
            return
        self.execute()
        self.finalize()
    @property
    def lhebanner(self):
        if not self._lhebanner:
            self._lhebanner = self.get_lhe_eventfile().get_banner()
        return self._lhebanner
    def get_lhe_eventfile(self, num = 0):
        exists = False
        for i, (lhe, exists) in enumerate(self.check_dataset("LHE", dataset_ext = ("tar.gz", "lhe.gz", "events"), verbose = False, use_fname = True).iteritems()):
            if i == num:
                break
        if exists:
            return lhe_parser.EventFile(lhe)
        else:
            raise IndexError('This event file {} does not exists.'.format(lhe))      
    def gen_xsection(self, mode = 'sum'):
        ret = {}
        if mode == 'sep':
            for l in (l.split() for l in self.lhebanner['init'].splitlines()[1:]):
                if l[0].startswith('<'):
                    break
                ret['P' + l[-1].strip()] = float(l[0])
            ret['total'] = sum(ret.values())
            return ret
        try:
            xsec = next(re.finditer(r"#  Integrated weight \(pb\)  :\s*(\S+)", self.lhebanner['mggenerationinfo'])).group(1)
        except:
            xsec = input("Can't find xsection from %s. Please assign one [pb]: " % os.path.relpath(self.output_path))
        ret['total'] = float(xsec)
        return ret

class PartonLevelSim(BasicSim):
    def __init__(self, process, slicer = "[:1000000:10000]", iseed = 21, dynamical_scale_choice = 3, *arg, **kwarg):
        super(PartonLevelSim, self).__init__(process, slicer = slicer, *arg, **kwarg)
        self.dirname = "LHE"
        self.check_output()
        self.dataset_ext = ("tar.gz", "lhe.gz", "events")
        self.iseed = iseed
        self.official = kwarg.get("official")
        self.dynamical_scale_choice = dynamical_scale_choice
        self.generator = "MG5"
        self.decay = True
        self.gen_mode = kwarg.get("gen_mode")
        self.use_MS = True if self.gen_mode=='MS' else False
        self.madspin = 'ON' if self.use_MS else 'OFF'
        self.madspin_path = os.path.join(self.run_path,'madspin')
        if not os.path.exists(self.madspin_path):
            os.makedirs(self.madspin_path)
        madspin_card = os.path.join(self.output_path, "madspin_card.dat")
        self.madspin_card = madspin_card if self.use_MS==True else ' '
        fcard = open(madspin_card,'w')
        fcard.write("""
#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event
#set Nevents_for_max_weigth 500
set ms_dir %s
set BW_cut 15
set seed 21
define j = g u c d s b u~ c~ d~ s~ b~
decay t > w+ b, w+ > all all 
decay t~ > w- b~, w- > all all
launch
        """%(self.madspin_path))
        fcard.close()
        self.param_card = kwarg.get("param_card", '/tmp/param_card.dat')
        self.dirname = "LHE"
        self._systematics = kwarg.get("systematics", "False")

    def initialise(self, check_only = False):
        if all(any(d.values()) for d in self.check_dataset(use_slicer = True, verbose = False).values()):
            try:
                self.process.read_param_card(self.lhebanner['slha'])
            except AttributeError:
                pass
            return "DONE"
        elif self.param_card == '/tmp/param_card.dat':
            pass
        if check_only:
            return "NOT DONE"
        self.check_output()
        do_RW = self.process.reweighted_from != None
        self.mg5_output = os.path.join(self.work_path, "PROC_{SIM}_gg{S}ttx{I}".format(SIM = "SIM" if not do_RW else "RW",
                                                                                    S = self.process.mediator,
                                                                                    I = "I" if self.process.signal_type == "SI" else ""))
        if os.path.exists(self.mg5_output):
            RunWeb = os.path.join(self.mg5_output, "RunWeb")
            if os.path.exists(RunWeb):
                os.remove(RunWeb)
            return
        matrixfname = "matrix_madevent_group_v4.inc" if not do_RW else "matrix_standalone_v4.inc"
        matrixf = os.path.join(c.MG5_path, "madgraph", "iolibs", "template_files", matrixfname)
        template_matrixf = os.path.join(c.root_path, "templates", ".".join([matrixfname] + ([] if self.process.signal_type != "SI" or not self.bkg_removal else ["SI"])))
        logger.debug('Replace "{}" with "{}".'.format(os.path.relpath(matrixf), os.path.relpath(template_matrixf)))
        shutil.copyfile(template_matrixf, matrixf)
        mg5_input_file = os.path.join(self.output_path, str(self.process) + ".mg5")
        with open(mg5_input_file, "w") as mg5_input:
            with open(os.path.join(c.root_path, "templates", "MG5_build_"+self.gen_mode+".mg5"), "r") as mg5_build:
                mg5_input.write(mg5_build.read().format(p = self.process, c = c, sim = self, mg5_output = self.mg5_output, commented = "#" if (do_RW or not self.decay) else "", standalone = "standalone" if do_RW else ""))
        with open(mg5_input_file, "r") as of:
            logger.info(of.read())
        mg5_executable = subprocess.Popen([os.path.join(c.MG5_path, "bin", "mg5_aMC"), mg5_input_file])
        mg5_executable.communicate()
        if (self.process.signal_type=="I"):
            modify_file = os.path.join(self.mg5_output, "cms_modify.sh"+("_reweight" if not do_RW else None))
            with open(modify_file, "w") as cms_input:
                with open(os.path.join(c.root_path, "cms_modify.sh"+("_reweight" if not do_RW else None)), "r") as cms_build:
                    if isinstance(self.process, Process.CMS_test):
                        cms_input.write(cms_build.read().format(path = self.mg5_output, couple = 85 if self.process.mediator=="A" else 86))
                    elif isinstance(self.process, Process.THDMaTtRes):
                        cms_input.write(cms_build.read().format(path = self.mg5_output, couple = 240 if self.process.mediator=="A" else 238))
                    else:
                        cms_input.write(cms_build.read().format(path = self.mg5_output, couple = 150 if self.process.mediator=="A" else 89))
            cms_test = subprocess.Popen(["bash", modify_file])
            cms_test.communicate()
        
        if mg5_executable.returncode:
            raise OSError("MG5 Error")
        # logger.info(str(mg5_executable.returncode))
        #subprocess.Popen([os.path.join(mg5_output, "bin", "generate_event"), mg5_input_file], stdout = sys.stdout, stdin = subprocess.PIPE).communicate()
    @property
    def systematics(self):
        if self._systematics is True:
            systematics = ["set systematics_arguments ['--pdf=errorset,CT14nlo@0,MMHT2014nlo68clas118@0', '--mur=0.5,1,2', '--muf=0.5,1,2', '--alps=0.5,1,2', '--together=mur,muf,dyn,alps']"]
        elif self._systematics is False:
            systematics = []
        else:
            systematics = self._systematics
        systematics.insert(0, "set use_syst {}".format(bool(self._systematics)))
        return "\n".join(systematics)
    @systematics.setter
    def systematics(self, option):
        self._systematics = option
    def execute(self):
        if self.param_card == '/tmp/param_card.dat':
            self.process.write_param_card(self.param_card)
        if self.official:
            event_folder = list(glob.iglob(os.path.join(self.mg5_output, 'Events', 'run_*')))
            if (len(event_folder)>3):
                raise OSError("Please remove the event folder before")
            mg5_input_file = os.path.join(self.output_path, str(self.process) + ".mg5")
            if (len(event_folder)==0):
                with open(mg5_input_file, "w") as mg5_input:
                    with open(os.path.join(c.root_path, "templates", "MG5_run.mg5"), "r") as mg5_build:
                        mg5_input.write(mg5_build.read().format(p = self.process, sim = self, c = c, mg5_output = self.mg5_output))
                with open(mg5_input_file, "r") as of:
                    logger.info(of.read())
                mg5_executable = subprocess.Popen([os.path.join(self.mg5_output, "bin", "madevent"), mg5_input_file])
                mg5_executable.communicate()
            shutil.rmtree(os.path.join(self.madspin_path))
            self.iseed = 20
            with open(mg5_input_file, "w") as mg5_input:
                with open(os.path.join(c.root_path, "templates", "MG5_run.mg5"), "r") as mg5_build:
                    mg5_input.write(mg5_build.read().format(p = self.process, sim = self, c = c, mg5_output = self.mg5_output))
                with open(mg5_input_file, "r") as of:
                    logger.info(of.read())
            mg5_executable = subprocess.Popen([os.path.join(self.mg5_output, "bin", "madevent"), mg5_input_file])
            mg5_executable.communicate()
            lhe = os.path.join(os.path.join(self.mg5_output, 'Events', 'run_01_decayed_1'), 'unweighted_events.lhe.gz')
            shutil.copy(lhe, os.path.join(self.output_path,"output_1.lhe.gz"))
            lhe = os.path.join(os.path.join(self.mg5_output, 'Events', 'run_02_decayed_1'), 'unweighted_events.lhe.gz')
            shutil.copy(lhe, os.path.join(self.output_path,"output_2.lhe.gz"))
        else:
            mg5_input_file = os.path.join(self.output_path, str(self.process) + ".mg5")
            with open(mg5_input_file, "w") as mg5_input:
                with open(os.path.join(c.root_path, "templates", "MG5_run.mg5"), "r") as mg5_build:
                    mg5_input.write(mg5_build.read().format(p = self.process, sim = self, c = c, mg5_output = self.mg5_output))
            with open(mg5_input_file, "r") as of:
                logger.info(of.read())
            mg5_executable = subprocess.Popen([os.path.join(self.mg5_output, "bin", "madevent"), mg5_input_file])
            mg5_executable.communicate()
            # logger.info(str(mg5_executable.returncode))
            event_folder = sorted(glob.iglob(os.path.join(self.mg5_output, 'Events', 'run_*')))[-1]
            lhe = lhe_parser.EventFile(os.path.join(event_folder, 'unweighted_events.lhe.gz'))
            for i, n in enumerate(self.slicer, self.slicer.start_index):
                sub_lhe_name = "{}._{:05d}.events".format(self.process, i+1)
                with open(os.path.join(event_folder, sub_lhe_name), 'w') as sub_lhe:
                #write the banner to the output file
                    sub_lhe.write(lhe.banner)
                    # Loop over all events
                    for en, event in enumerate(lhe):
                        sub_lhe.write(str(event))
                        if en+1 == self.slicer.step:
                            break
                    sub_lhe.write('</LesHouchesEvent>\n')
                with tarfile.open(os.path.join(self.output_path, sub_lhe_name.replace('.events', '.' + self.dataset_ext[0])),'w:gz') as lhe_arxiv:
                    lhe_arxiv.add(sub_lhe.name, sub_lhe_name, recursive = False)
                os.remove(sub_lhe.name)

    def finalize(self):
        os.chdir(c.root_path)
        '''
        try:
            shutil.rmtree(os.path.join(self.madspin_path))
        except OSError as e:
            logger.error('removing Madspin directory fail!')
        try:
            shutil.rmtree(os.path.join(self.work_path))
        except OSError as e:
            logger.error('removing working directory fail!')
            logger.error('Please remove it maullay by doing:\n\trm -rf {}\n or this could cause issues later.'.format(self.work_path))
            raise e
        '''
        # subprocess.check_output([os.path.join(self.mg5_output, "bin", "cleanall")])
    def validate(self, use_slicer = False, ignore_error = False, match = None, matrix_element = True, matching_strategy = "safe", **kwarg):
        import ROOT
        if "AOD" in match or match=="EXOT":
            check_rc()
            utils.rcSetup(os.path.join(c.root_path, "Core", "AnalysisTop"))
        vector = ROOT.vector
        tv = ROOT.TLorentzVector
        if matrix_element:
            ME = BasicRW(self.process, ebeam1 = self.ebeam1, ebeam2 = self.ebeam2)
            ME.output_path = self.output_path
            ME.initialise()
        # Start Tree Header
        header = {"LHEF": ROOT.TNamed("LHEF", ""),
                  match:  ROOT.TNamed(match or "NOMATCH", "")}
        branches = {
                    "t":              vector(   tv)(),
                    "tx":             vector(   tv)(),
                    "b":              vector(   tv)(),
                    "bx":             vector(   tv)(),
                    "w1":             vector(   tv)(),
                    "w2":             vector(   tv)(),
                    "g1":             vector(   tv)(),
                    "g2":             vector(   tv)(),
                    "aS":             vector(float)(),
                    "ME":             vector(float)(),
                    }
        # End Tree Header
        first_file = True
        m2list0 = list(glob.glob(os.path.join(self.run_path, match, '*{}*'.format(match[:-1] if 'DAOD_TRUTH' in match else match)))) if match else ['']
        for m20 in m2list0:
            additional = '.'.join(m20.split('/')[-1].split('.')[1:3])
            m2list = [m20]
            m1list = []
            for dname, exts in self.check_dataset(inplace = False, use_slicer = use_slicer, use_fname = False, search = True).iteritems():
                skip = False
                for ext, is_exist in exts.iteritems():
                    logger.info('Runnig: {}'.format(dname))
                    output = os.path.join(self.output_path, dname + "." + ext)
                    m1list.append(output)
                    logger.info('Output path: {}'.format(output))
            ntup = m20.replace("/EXOT/", "/LHE/").replace('.pool.root.1','.LHEDAOD.root.tmp')
            if os.path.exists(ntup.rsplit('.tmp', 1)[0]):
                skip = True
            if skip:
                logger.info('Output exists. SKIP!')
                continue
            if not (is_exist or ignore_error):
                raise OSError("Data({output}) does not exist. Please check!".format(output = output))
            logger.info('Safe matching mode. All events are put into matching at once')
            pat = os.path.join(self.run_path, match, '*{}*'.format(match+"4_"+additional))
            logger.info('Searh pattern for files being matched to: "{}"'.format(pat))
            logger.debug('Matching')
            logger.debug('\tData({output})'.format(output = m1list))
            logger.debug('to')
            logger.debug('\tData({})'.format(m20))

            Core.EventMatching.Events.logger.setLevel(logging.INFO)
            get_matched = matching(m1list, m2list, "LHE", "AOD" if "AOD" in match else match)
            ntup = ROOT.TFile(ntup, "RECREATE")
            new_tree = ROOT.TTree("CollectionTree", "CollectionTree")
            for branchname, address in branches.iteritems():
                new_tree.Branch(branchname, address)
            LHE_number  = array('i',[0])
            eventNumber  = array('i',[0])
            new_tree.Branch("LHE_number", LHE_number, "LHE_number/I")
            new_tree.Branch("eventNumber", eventNumber, "eventNumber/I")
            for m1 in m1list:
                header["LHEF"].SetTitle(m1)
            header[match].SetTitle(m2list[0])
            lhe_file = lhe_parser.EventFile(output)
            if matrix_element and first_file:
                first_file = False
                ME.set_param_card(lhe_file.get_banner()['slha'])
            chain = ROOT.TChain("CollectionTree")
            chain.Add(m2list[0])
            tree = ROOT.xAOD.MakeTransientTree(chain)

            for en in xrange(tree.GetEntriesFast()):
                if en % 5000 == 0:
                    logger.info("#{}".format(en))
                eventNumber[0] = -555
                LHE_number[0] = -555
                is_matched, eventNumbers, LHE_No, t_vec, tx_vec, g1_vec, g2_vec, w1_vec, w2_vec, b_vec, bx_vec, aS = next(get_matched)
                if is_matched:
                    eventNumber[0] = eventNumbers[0]
                    LHE_number[0] = LHE_No[0]
                    branches["t"].push_back(t_vec)
                    branches["tx"].push_back(tx_vec)
                    branches["g1"].push_back(g1_vec)
                    branches["g2"].push_back(g2_vec)
                    branches["w1"].push_back(w1_vec)
                    branches["w2"].push_back(w2_vec)
                    branches["b"].push_back(b_vec)
                    branches["bx"].push_back(bx_vec)
                    branches["aS"].push_back(aS)
                    branches["ME"].push_back(ME._execute(branches) if matrix_element else 0.)
                new_tree.Fill()
                for branchname, address in branches.iteritems():
                    address.clear()
            ntup.cd()
            for name in header.values():
                name.Write()
            ntup.Write()
            ntup.Close()
            shutil.move(ntup.GetName(), ntup.GetName().rsplit('.tmp', 1)[0])
            lhe_file.close()
        if matrix_element:
            ME.finalize()

class TruthLevelSim(BasicSim):
    def __init__(self, process, slicer = "[:1000000:5000]", *arg, **kwarg):
        super(TruthLevelSim, self).__init__(process, slicer = slicer, *arg, **kwarg)
        self.dataset_ext = ["DAOD_TRUTH.root"]
        self.generator = "MadGraph5+Pythia6"
        self.dirname = c.TRUTH_FMT
        self.evgenJobOpts = kwarg.get("evgenJobOpts", None)
    def initialise(self):
        self.check_output()
        self.evnt_dir = os.path.join(self.output_path, '..', "EVNT")
        if not os.path.exists(self.evnt_dir):
            os.makedirs(self.evnt_dir)
        self.athena = os.path.join(c.root_path, "Core", "Athena")
    def execute(self):
        outputs = map(short_name, self.check_dataset(inplace = False).iterkeys())
        firstEvent = 0
        for dataset, exts in self.check_dataset(dirname = "LHE", dataset_ext = ["tar.gz"], inplace = False).iteritems():
            for ext, is_exist in exts.iteritems():
                if is_exist:
                    inputGeneratorFile = os.path.join(self.output_path, '..', "LHE", ".".join([dataset, ext]))
            short = short_name(inputGeneratorFile)
            add = inputGeneratorFile.split('/')[-1]
            dsid, ipart = add.split("._")
            ipart = ipart.split('.')[0]
            ipart = ipart[len(ipart) - 3:]
            if short in outputs: 
                continue
            subwork_path = os.path.join(self.work_path, short)
            try:
                os.makedirs(subwork_path)
            except OSError:
                pass
            outputEVNTFile = os.path.join(self.evnt_dir, os.path.basename(inputGeneratorFile.split(".tar")[0]) + ".EVNT.root")
            with open(os.path.join(c.root_path, "templates", "Athena_run.sh"), "r") as Athena_run_template:
                Athena_run_template = Athena_run_template.read().format(root_path = c.root_path,
                                                                        work_path = subwork_path,
                                                                        output_path = self.output_path,
                                                                        process = self.process,
                                                                        run_number = self.process.dataset_number if str(self.process.dataset_number).isdigit() else "999999",
                                                                        jo = self.find_jo(inputGeneratorFile, evgenJobOpts = self.evgenJobOpts),
                                                                        outputEVNTFile = outputEVNTFile,
                                                                        inputGeneratorFile = inputGeneratorFile,
                                                                        outputFile = os.path.join(self.output_path, os.path.basename(inputGeneratorFile.split(".tar")[0]) + ".{}".format(self.dataset_ext[0])),
                                                                        fmt = self.dirname.split('_'),
                                                                        evgenJobOpts = ("evgenJobOpts=" + self.evgenJobOpts) if self.evgenJobOpts else "",
                                                                        firstEvent = firstEvent,
                                                                        Athena = self.athena,
                                                                        ecmEnergy = self.ebeam1 + self.ebeam2)
                with open(os.path.join(subwork_path, "p{}.{}".format(dsid, ipart)), "w") as Athena_run:
                    logger.info(Athena_run_template)
                    Athena_run.write(Athena_run_template)
            if self.use_cluster:
                if not os.path.exists(os.path.join(c.root_path, 'submissionWorkloadTemp')):
                    os.makedirs(os.path.join(c.root_path, 'submissionWorkloadTemp'))
                subprocess.call(['cp', os.path.join(subwork_path, "p{}.{}".format(dsid, ipart)),os.path.join(c.root_path, 'submissionWorkloadTemp/.')])
                subprocess.check_call(['chmod', '+x', Athena_run.name])
                job_id = self.cluster.get_identifier()
                self.cluster.submit(Athena_run.name,
                                       stdout = os.path.join(self.log_path, 'out.%s' % job_id),
                                       stderr = os.path.join(self.log_path, 'out.%s' % job_id),
                                       log    = os.path.join(self.log_path, 'log.%s' % job_id),
                                       # requirement = 'Requirements = ( OpSysAndVer == "SL6" )'
                                       )
            else:
                subprocess.check_output(['sh ' + Athena_run.name], shell = True, executable="/bin/zsh")
        if self.use_cluster:
            self.cluster.wait(None, fct = get_fct(logger = logger))
    def find_jo(self, inputGeneratorFile, evgenJobOpts = None):
        evgenJobOpts = evgenJobOpts or self.evgenJobOpts
        run_number = os.path.split(inputGeneratorFile)[1].split(".")[3]
        jo = glob.glob(os.path.join(c.root_path, "TestArea-MC12JobOptions-00-16-76", "MC12JobOptions-00-16-76", "share", "DSID{:x<6.3}".format(run_number) , "*." + run_number + ".*.py"))
        if jo:
            if evgenJobOpts:
                return os.path.join("MC12JobOptions", os.path.split(jo[0])[1])
            else:
                return jo[0]
        else:
            return os.path.join(self.athena, "mc.MGPy8EG_tmp.py")
    def validate(self, use_slicer = False, ignore_error = False, new_weights = None, algorithms = ['SL'], **kwargs):
        check_rc()
        import ROOT
        vector = ROOT.vector
        tv = ROOT.TLorentzVector
        if (new_weights == None) and (self.process.reweighted_from != None):
            rw = TruthLevelRW(self.process, slicer = self.slicer, ebeam1 = self.ebeam1, ebeam2 = self.ebeam2)
            rw.initialise()
            new_weights = rw.execute(use_slicer = use_slicer)
            PreSim = rw.PreSim
        else:
            PreSim = self
        if "DAOD" in self.dataset_ext[0]:
            utils.rcSetup(os.path.join(c.root_path, "Core", "AnalysisTop"))
        # Start Tree Header
        branches = {
                    "weight":         vector(float)(),
                    "t":              vector(   tv)(),
                    "t_W":            vector(   tv)(),
                    "t_b":            vector(   tv)(),
                    "t_j":            vector(   tv)(),
                    "t_jx":           vector(   tv)(),
                    "t_hasD":         vector(  int)(),
                    "t_hasS":         vector(  int)(),
                    "t_hasElectron":  vector(  int)(),
                    "t_hasMuon":      vector(  int)(),
                    "t_hasTau":       vector(  int)(),
                    "t_hasB":         vector(  int)(),
                    "tx":             vector(   tv)(),
                    "tx_W":           vector(   tv)(),
                    "tx_b":           vector(   tv)(),
                    "tx_j":           vector(   tv)(),
                    "tx_jx":          vector(   tv)(),
                    "tx_hasD":        vector(  int)(),
                    "tx_hasS":        vector(  int)(),
                    "tx_hasElectron": vector(  int)(),
                    "tx_hasMuon":     vector(  int)(),
                    "tx_hasTau":      vector(  int)(),
                    "tx_hasB":        vector(  int)(),
                    "ttx":            vector(   tv)(),
                    "ev_hasRec":      vector(  int)(),
                    "t_e":            vector(   tv)(),
                    "t_enu":          vector(   tv)(),
                    "t_mu":           vector(   tv)(),
                    "t_munu":         vector(   tv)(),
                    "t_tau":          vector(   tv)(),
                    "t_taunu":        vector(   tv)(),
                    "tx_e":           vector(   tv)(),
                    "tx_enu":         vector(   tv)(),
                    "tx_mu":          vector(   tv)(),
                    "tx_munu":        vector(   tv)(),
                    "tx_tau":         vector(   tv)(),
                    "tx_taunu":       vector(   tv)(),
                    "add_pt_l_l":     vector(float)(),
                    "add_pt_l_j":     vector(float)(),
                    "add_pt_sl_j":    vector(float)(),
                    "add_number_jet": vector(  int)(),
                    "jets":           vector(   tv)(),
                    }
        for branchname, address in branches.iteritems():
            if ("_has" in branchname) or ("add" in branchname):
                address.push_back(0)
        if (self.process.reweighted_from == None) and all(self.check_dataset(dataset_ext = ['NTUP_TRUTH.root'], use_fname = True, use_slicer = False).values() or (False, )):
            logger.info("DONE")
            return
        ntup = os.path.join(self.output_path, self.process.__str__(True) + (".NTUP_TRUTH_RW.root" if self.process.reweighted_from != None else ".NTUP_TRUTH.root"))
        ntup = ROOT.TFile(ntup, "RECREATE")
        new_tree = ROOT.TTree("CollectionTree", "CollectionTree")
        Algorithm = __import__("Algorithm")
        algorithms = [getattr(Algorithm, alg)() for alg in algorithms]
        for alg in algorithms:
            branches.update(alg.branches)
        for branchname, address in branches.iteritems():
            new_tree.Branch(branchname, address)
        # End Tree Header
        chain = ROOT.TChain("CollectionTree")
        for output, exts in PreSim.check_dataset(dirname = self.dirname, dataset_ext = self.dataset_ext, inplace = False, use_slicer = use_slicer).iteritems():
            for ext, is_exist in exts.iteritems():
                output = os.path.join(PreSim.run_path, self.dirname, output + "." + ext)
                if is_exist:
                    break
            if not (is_exist or ignore_error):
                raise OSError("Data({output}) does not exist. Please check!".format(output = output))
            chain.Add(output)
        tree = ROOT.xAOD.MakeTransientTree(chain)
        for alg in algorithms:
            alg.set_tree(tree)
        for en in xrange(tree.GetEntriesFast()):
            if en % 5000 == 0:
                logger.info("#{}".format(en))
            tree.GetEntry(en)
            branches["weight"].push_back(tree.EventInfo.mcEventWeight(0) * (next(new_weights) if new_weights else 1.))
            nt = 0
            ntx = 0
            for top in tree.TruthParticles:
                if top.isTop():
                    name = "t_" if top.pdgId() == 6 else "tx_"
                    if top.pdgId() == 6:
                        nt = nt+1
                        if nt == 2:
                           nt = 1
                           continue
                    if top.pdgId() == -6:
                        ntx = ntx+1
                        if ntx == 2:
                           ntx = 1
                           continue
                    for i, p in enumerate(get_decay_chain(top)):
                        branches[category(p, name)].push_back(p.p4()*0.001)
                        for pid, lep in zip((11, 13, 15, 1, 3, 5), ("Electron", "Muon", "Tau", "D", "S", "B")):
                            if abs(p.pdgId()) == pid and (p.status()==23):
                                branches[name + "has" + lep][0] += 1
                                break
                if top.isTop():
                    name = "t" if top.pdgId() == 6 else "tx"
                    for i, p in enumerate(get_decay_chain(top)):
                        for pid, lep in zip((11, 13, 15, 12, 14, 16), ("e", "mu", "tau", "enu", "munu", "taunu")):
                            if abs(p.pdgId()) == pid and (p.status()==23):
                                branches[name+"_"+lep].push_back(p.p4()*0.001)
                if (nt != 0 and ntx != 0):
                    break
            branches["ttx"].push_back(branches["t"][0] + branches["tx"][0])

            # selections
            for alg in algorithms:
                branches['ev_hasRec'][0] = alg()

            l_pass = []
            for lept in tree.TruthElectrons:
                l_pass.append(lept)
            for lept in tree.TruthMuons:
                l_pass.append(lept)
            for lept in tree.TruthTaus:
                l_pass.append(lept)
            pt_leading_l = 0
            for l in l_pass:
                if (l.pt() > pt_leading_l):
                    pt_leading_l = l.pt()
            branches["add_pt_l_l"][0] = pt_leading_l*0.001

            pt_j = []
            for j in tree.AntiKt4TruthDressedWZJets:
                pt_j.append(j.pt())
            pt_j.sort()
            if len(pt_j) >= 1:
                branches["add_pt_l_j"][0] = pt_j[len(pt_j)-1]*0.001
                if len(pt_j) >= 2:
                    branches["add_pt_sl_j"][0] = pt_j[len(pt_j)-2]*0.001
            branches["add_number_jet"][0] = len(pt_j)

            new_tree.Fill()
            for branchname, address in branches.iteritems():
                if ("_has" in branchname) or ("chi2" in branchname) or ("add" in branchname):
                    address[0] = 0
                    continue
                address.clear()
        ntup.Write()
        ntup.Close()
        if (new_weights == None) and (self.process.reweighted_from != None):
            rw.finalize()
        ROOT.xAOD.ClearTransientTrees()
        chain.GetFile().Close()

class NewLevelSim(BasicSim):
    def __init__(self, process, slicer = "[:1000000:5000]", *arg, **kwarg):
        super(NewLevelSim, self).__init__(process, slicer = slicer, *arg, **kwarg)
        self.dataset_ext = ["DAOD_EXOT"]
        self.dirname = "EXOT"
        self.check_output()

    def validate(self, use_slicer = False, ignore_error = False, new_weights = None, **kwargs):
        check_rc()
        import ROOT
        vector = ROOT.vector
        if (new_weights == None) and (self.process.reweighted_from != None):
            rw = NewLevelRW(self.process, slicer = self.slicer, ebeam1 = self.ebeam1, ebeam2 = self.ebeam2)
            rw.initialise()
            PreSim = rw.PreSim
        else:
            PreSim = self
        if "DAOD" in self.dataset_ext[0]:
            utils.rcSetup(os.path.join(c.root_path, "Core", "AnalysisTop"))
        chain = ROOT.TChain("CollectionTree")
        tree_match = ROOT.TChain("CollectionTree")
        for output, exts in PreSim.check_dataset(dirname = self.dirname, dataset_ext = self.dataset_ext, inplace = False, use_slicer = use_slicer, search = True).iteritems():
            for ext, is_exist in exts.iteritems():
                output = os.path.join(PreSim.run_path, self.dirname, ext+output)
                print(output)
                if is_exist:
                    break
            if not (is_exist or ignore_error):
                raise OSError("Data({output}) does not exist. Please check!".format(output = output))
            chain.Add(output)
            tree = ROOT.xAOD.MakeTransientTree(chain)
            output_match = output.replace("/EXOT/", "/LHE/").replace('.pool.root.1','.LHEDAOD.root')
            tree_match.Add(output_match)
        tree.GetEntry(1)
        ntup = os.path.join(self.output_path, str(int(tree.EventInfo.mcChannelNumber()))+"_"+self.process.reweighted_from.mediator+str(int(self.process.reweighted_from.mass))+self.process.reweighted_from.signal_type+str(self.process.reweighted_from.tanb)+"_to_"+self.process.mediator+str(int(self.process.mass))+self.process.signal_type+str(self.process.tanb)+"_weight.root")
        ntup = ROOT.TFile(ntup, "RECREATE")
        new_tree = ROOT.TTree("CollectionTree","CollectionTree")
        new_weight  = array('f',[0])
        eventNumber  = array('i',[0])
        mcChannelNumber  = array('i',[0])
        wgt_br = "w_"+self.process.mediator+str(int(self.process.mass))+self.process.signal_type+str(self.process.tanb)
        new_tree.Branch(wgt_br, new_weight, wgt_br+"/F")
        new_tree.Branch("eventNumber", eventNumber, "eventNumber/I")
        new_tree.Branch("mcChannelNumber", mcChannelNumber, "mcChannelNumber/I")
        for en in xrange(tree.GetEntriesFast()):
            if en % 5000 == 0:
                logger.info("#{}".format(en))
            tree.GetEntry(en)
            tree_match.GetEntry(en)
            new_weight[0] = tree.EventInfo.mcEventWeight(0)*rw.execute(tree_match)/tree_match.ME[0]
            eventNumber[0] = tree.EventInfo.eventNumber()
            mcChannelNumber[0] = tree.EventInfo.mcChannelNumber()
            if tree.EventInfo.eventNumber()!=tree_match.eventNumber:
                raise IndexError('EventNumber in DAOD and LHE does not match.')    
            new_tree.Fill()
        ntup.Write()
        ntup.Close()
        if (new_weights == None) and (self.process.reweighted_from != None):
            rw.finalize()
        ROOT.xAOD.ClearTransientTrees()
        chain.GetFile().Close()
        tree_match.GetFile().Close()

class BasicRW(PartonLevelSim):
    """Basic RWGT Tools
    
    These classes is only used to compute Matrix Elements, which means it behaves totally different from a __BasicSim__ instance.
    """
    def __init__(self, process, param_card = None, *arg, **kwarg):
        super(BasicRW, self).__init__(process, *arg, **kwarg)
        self.param_card = param_card
        self.matrices = []
        self.PreSim = PartonLevelSim(self.process.reweighted_from or self.process, ebeam1 = self.ebeam1, ebeam2 = self.ebeam2, slicer = self.slicer)
        self.dataset_ext = ["NTUP_LHE.root"]
        self.generator = "RWGT from {p.mass}GeV {p.mediator} tan#beta={p.tanb}".format(p = self.process.reweighted_from or self.process)
        self.me_args = (0,)
        self.decay = True
    def _initialise(self):
        global ROOT
        import ROOT
        shutil.copyfile(os.path.join(c.root_path,"ident_{}.dat".format(self.process.models)), os.path.join(c.root_path,"ident_card.dat"))
        self.check_output()
        self.mg5_output = os.path.join(self.work_path, "PROC_{SIM}_gg{S}ttx{I}".format(SIM = "RW",
                                                                                       S = self.process.mediator,
                                                                                       I = "I" if self.process.signal_type == "SI" else ""))
        if '[QCD]' in self.process.sim_config["coupling_order"]:
            self.process.sim_config["coupling_order"] = self.sim_config["coupling_order"].replace('[QCD]','[virt=QCD]')
            self.me_args = (91.2, -1)
        if os.path.exists(self.mg5_output):
            RunWeb = os.path.join(self.mg5_output, "RunWeb")
            if os.path.exists(RunWeb):
                os.remove(RunWeb)
            return
        matrixfname = "matrix_standalone_v4.inc"
        matrixf = os.path.join(c.MG5_path, "madgraph", "iolibs", "template_files", matrixfname)
        template_matrixf = os.path.join(c.root_path, "templates", ".".join([matrixfname] + ([] if self.process.signal_type != "SI" or not self.bkg_removal else ["SI"])))
        logger.debug('Replace "{}" with "{}".'.format(os.path.relpath(matrixf), os.path.relpath(template_matrixf)))
        shutil.copyfile(template_matrixf, matrixf)
        mg5_input_file = os.path.join(self.output_path, str(self.process) + ".mg5")
        with open(mg5_input_file, "w") as mg5_input:
            with open(os.path.join(c.root_path, "templates", "MG5_build_ME.mg5"), "r") as mg5_build:
                mg5_input.write(mg5_build.read().format(p = self.process, sim = self, mg5_output = self.mg5_output, commented = "" if self.decay else "#", standalone = "standalone"))
        with open(mg5_input_file, "r") as of:
            logger.info(of.read())
        mg5_executable = subprocess.Popen([os.path.join(c.MG5_path, "bin", "mg5_aMC"), mg5_input_file])
        mg5_executable.communicate()
        if (self.process.signal_type=="I"):
            modify_file = os.path.join(self.mg5_output, "cms_modify.sh_reweight")
            with open(modify_file, "w") as cms_input:
                with open(os.path.join(c.root_path, "cms_modify.sh_reweight"), "r") as cms_build:
                    if isinstance(self.process, Process.CMS_test):
                        cms_input.write(cms_build.read().format(path = self.mg5_output, couple = 85 if self.process.mediator=="A" else 86))
                    elif isinstance(self.process, Process.THDMaTtRes):
                        cms_input.write(cms_build.read().format(path = self.mg5_output, couple = 240 if self.process.mediator=="A" else 238))
                    else:
                        cms_input.write(cms_build.read().format(path = self.mg5_output, couple = 150 if self.process.mediator=="A" else 89))
            cms_test = subprocess.Popen(["bash", modify_file])
            cms_test.communicate()
        # logger.info(str(mg5_executable.returncode))
    def initialise(self):
        self._initialise()
        for subProcess in glob.iglob(os.path.join(self.mg5_output, "SubProcesses", "P*_*")):
            temp = next(glob.iglob(os.path.join(subProcess, "matrix*py.so")), None)
            if not temp:
                temp = next(tempfile._get_candidate_names())
                try:
                    subprocess.check_output(["make", "-C", subProcess, "SHELL=" + c.shell, "matrix%spy.so" % temp, "-e", "MENUM=%s" % temp])
                except subprocess.CalledProcessError as e:
                    logger.error('Compilation of the ME calculators fail! This usually happens when the working directory was not completely deleted in the previous run.')
                    raise e
                temp = "matrix%spy" % temp
            else:
                temp = os.path.splitext(os.path.basename(temp))[0]
            m = imp.load_module(temp , *imp.find_module(temp, [subProcess]))
            self.matrices.append(m)
        # os.chdir(subProcess)
    def set_param_card(self, param_card = None):
        if param_card is not None:
            if os.path.exists(param_card):
                param_card = os.path.abspath(param_card)
                with open(param_card) as rf:
                    param_card = rf.read()
        self.param_card = param_card
        # why I cd to subprocess directory? I don't remember it at all. It seems relate to loop-induced process.
        os.chdir(next(glob.iglob(os.path.join(self.mg5_output, "SubProcesses", "P*_*"))))

        #with tempfile.NamedTemporaryFile(dir = os.path.join(self.mg5_output, "Cards"), suffix = ".dat") as param_card:
        with open(os.path.join(self.mg5_output, "Cards","param_card.dat"), "w+") as param_card:
            if not self.param_card:
                self.process.write_param_card(param_card.name)
            else:
                param_card.write(self.param_card)
            param_card.seek(0)
            for m in self.matrices:
                initialisemodel(m)(param_card.name)
            self.param_card = param_card.read()
    def _execute(self, ev, slicer = slice(None)):
        return sum(get_me(m)([[ev["g1"][0].E(), ev["g2"][0].E() , ev["b"][0].E() , ev["w1"][0].E() , ev["bx"][0].E() , ev["w2"][0].E() ],
                             [ev["g1"][0].Px(), ev["g2"][0].Px(), ev["b"][0].Px(), ev["w1"][0].Px(), ev["bx"][0].Px(), ev["w2"][0].Px()],
                             [ev["g1"][0].Py(), ev["g2"][0].Py(), ev["b"][0].Py(), ev["w1"][0].Py(), ev["bx"][0].Py(), ev["w2"][0].Py()],
                             [ev["g1"][0].Pz(), ev["g2"][0].Pz(), ev["b"][0].Pz(), ev["w1"][0].Pz(), ev["bx"][0].Pz(), ev["w2"][0].Pz()]
                             ],
                            ev["aS"][0], *self.me_args) for m in self.matrices[slicer])


class TruthLevelRW(BasicRW):
    def __init__(self, process, slicer = "[:1000000:5000]", param_card = None, *arg, **kwarg):
        super(TruthLevelRW, self).__init__(process, param_card, slicer = slicer, *arg, **kwarg)
        self.dataset_ext = ("NTUP_TRUTH.root")
        self.slicer = slicer
    def initialise(self):
        super(TruthLevelRW, self).initialise()
        self.set_param_card()
    def set_param_card(self, param_card = None):
        if not (param_card or self.param_card):
            #with tempfile.NamedTemporaryFile(dir = os.path.join(self.mg5_output, "Cards")) as param_card:
            with open(os.path.join(self.mg5_output, "Cards","param_card.dat"), "w+") as param_card:
                self.process.write_param_card(param_card.name)
                param_card.seek(0)
                for m in self.matrices:
                    initialisemodel(m)(param_card.name)
                self.param_card = param_card.read()
    def execute(self, use_slicer = False):
        for fname, is_exist in self.PreSim.check_dataset(dirname = "LHE", dataset_ext = ("NTUP_LHE.root",), use_slicer = use_slicer, use_fname = True).iteritems():
            if is_exist:
                f = ROOT.TFile(fname)
                ev = f.CollectionTree
                for en in xrange(ev.GetEntries()):
                    ev.GetEntry(en)
                    if ev.ev_hasShowered[0]:
                        yield sum(get_me(m)([[ev.g1[0].E(),  ev.g2[0].E(),  ev.t[0].E(),  ev.tx[0].E()],
                                            [ev.g1[0].Px(), ev.g2[0].Px(), ev.t[0].Px(), ev.tx[0].Px()],
                                            [ev.g1[0].Py(), ev.g2[0].Py(), ev.t[0].Py(), ev.tx[0].Py()],
                                            [ev.g1[0].Pz(), ev.g2[0].Pz(), ev.t[0].Pz(), ev.tx[0].Pz()]
                                           ],
                                           ev.aS[0],
                                           0) for m in self.matrices) / ev.ME[0]

class NewLevelRW(BasicRW):
    def __init__(self, process, slicer = "[:1000000:5000]", param_card = None, *arg, **kwarg):
        super(NewLevelRW, self).__init__(process, param_card, slicer = slicer, *arg, **kwarg)
        self.dataset_ext = ("DAOD_EXOT")
        self.slicer = slicer
        self.dirname = "EXOT"
    def initialise(self):
        super(NewLevelRW, self).initialise()
        self.set_param_card()
    def set_param_card(self, param_card = None):
        if not (param_card or self.param_card):
            #with tempfile.NamedTemporaryFile(dir = os.path.join(self.mg5_output, "Cards")) as param_card:
            with open(os.path.join(self.mg5_output, "Cards","param_card.dat"), "w+") as param_card:
                self.process.write_param_card(param_card.name)
                param_card.seek(0)
                for m in self.matrices:
                    initialisemodel(m)(param_card.name)
                self.param_card = param_card.read()
    def execute(self, ev, use_slicer = False):
        return sum(get_me(m)([[ev.g1[0].E(),  ev.g2[0].E(),  ev.t[0].E(),  ev.tx[0].E()],
                              [ev.g1[0].Px(), ev.g2[0].Px(), ev.t[0].Px(), ev.tx[0].Px()],
                              [ev.g1[0].Py(), ev.g2[0].Py(), ev.t[0].Py(), ev.tx[0].Py()],
                              [ev.g1[0].Pz(), ev.g2[0].Pz(), ev.t[0].Pz(), ev.tx[0].Pz()]
                              ],
                              ev.aS[0],
                              0) for m in self.matrices)
