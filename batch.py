from datetime import date
from subprocess import call
import argparse
import os
cwd = os.getcwd()
fp = "/afs/cern.ch/work/c/caiyi/condor/"

#parser = argparse.ArgumentParser()
#parser.add_argument('-n', '--run_number', type=str)
#parser.add_argument('-e', '--ecm', type=str)
#parser.add_argument('-m', '--model', type=str)
#result = parser.parse_args()

aorh = ['H']
mass = ['400','500','600','700','800','900']
sori = ['S','SI']
Event_slice = '\"[:1600000:10000]\"'
ecm = '13000'
model = '2HDM'
reweight = [
            '--rw_mass 400',
           # '--rw_mass 500',
           # '--rw_mass 600',
           # '--rw_mass 700',
           # '--rw_mass 800',
           # '--rw_mass 900',
           ]

def makecommand(m,tanb,si,ah):
    commands = []
    for weight in reweight:
        runstr = cwd + "/main.py parameters {:s} {:s} {:s} {:s} {:s} 100 0 --model {:s} -e {:s} -nv --reweight {:s}\n".format(ah, m, tanb, si, Event_slice, model, ecm, weight)
        commands.append(runstr)
    return commands

for ah in aorh:
    for si in sori:
        if (si == 'S'):
            tanbeta = ['1.0','1.5','2.0']
        elif (si == 'SI'):
            tanbeta = ['0.4','1.0','1.5','2.0']
        for m in mass:
            for tanb in tanbeta:
                commands = makecommand(m,tanb,si,ah)

                execstr = ""
                execstr += model+'_'+ah+m+si+tanb+'_'+ecm
                execstr += "_reweight"

                tmp = """#!/bin/sh
                function setupATLAS() {
                    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
                    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
                }
                setupATLAS"""+"""
                cd {:s}

                echo Now set up environment
                source {:s}/setup.sh

                echo Now running
                """.format(cwd,cwd)

                submitstr = """#!/bin/tcsh
                #
                executable              = %s.sh
                arguments               = $(ClusterId)$(ProcId)
                output                  = %s.$(ClusterId).$(ProcId).out
                error                   = %s.$(ClusterId).$(ProcId).err
                log                     = %s.$(ClusterId).log
                +JobFlavour             = "workday"
                RequestCpus             = 1
                queue
                """

                bsubscript = tmp+'\n'
                for com in commands:
                    bsubscript=bsubscript+com
                submitbsub = submitstr % (execstr+'_cmd',execstr,execstr,execstr)
                exc = open(fp+execstr+'_exc.sub', 'w')
                exc.write(submitbsub)
                exc.close()
                exc = open(fp+execstr+'_cmd.sh', 'w')
                exc.write(bsubscript)
                exc.close()
                call(['chmod', '744', fp+execstr+'_exc.sub'])
                call(['chmod', '744', fp+execstr+'_cmd.sh'])

                excscript = open(fp+'runBatch.sh', 'w')
                excscript.write('cd '+fp+'\n')
                excscript.write('condor_submit '+execstr+'_exc.sub\n')
                excscript.write('cd '+cwd+'\n')
                excscript.close()

                print(fp+execstr+'_cmd.sh')
                call(['bash', os.path.join(fp,'runBatch.sh')])
