# 2HDM A/H > tt~ (+Interf) event generation/reweighting
The code is used to produce the signal-plus-interference pattern of BSM Higgs produced in the top-antitop quark pair final states in either a generic 2HDM or 2HDM+a model.

Prerequisites
-------------
#### Necessary
*   MadGraph >= 2.6.0
*   AanalysisTop,21.2.87 or similar
    - SLC6 environment is recommended to avoid conflicts due to the python2.7->3.0 transition.
*   HTCondor or similar batch systems
*   UFO model files:
    -    Generic 2HDM: ![Higgs_Effective_Couplings_FormFact]
    -    2HDM+a: ![Pseudoscalar_2HDM_FormFactor]

[Higgs_Effective_Couplings_FormFact]: https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/Models/ggHFullLoop
[Pseudoscalar_2HDM_FormFactor]: https://gitlab.cern.ch/yuchen/Pseudoscalar_2HDM_FormFactor

#### Optional
*   IPython (for debugging)
*   coloredlogs (for debugging)

Setup
-----
1.  setupATLAS
    ```bash
    setupATLAS
    ```
2.  `git clone` this package
    ```bash
    git clone --recurse-submodules https://gitlab.cern.ch/caiyi/2HDM_SI.git
    git checkout MS_new
    ```
3.  Modify `config.py` to adapt to your needs
4.  source `setup.sh` every time whenever you log in.
    ```bash
    source ./setup.sh
    ```

Usage
-----
`./main.py --help` to show this help message:
```bash
usage: main.py [-h] [-i ISEED] [-e ECM] [--param_card PARAM_CARD]
               [-p [sim_opt [sim_opt ...]]] [-pv [sim_opt [sim_opt ...]]]
               [-t [sim_opt [sim_opt ...]]] [-tv [sim_opt [sim_opt ...]]]
               [-c CLUSTER] [--ffmt FFMT] [--model {2HDM,2HDM+a}]
               [-s SYSTEMATICS]
               {parameters} ...

MadgraphPythia Type-II 2HDM gg > H/A > tt~ (+ Interf) production.

positional arguments:
  {parameters}
    parameters          Specify a process using physical parameters.

optional arguments:
  -h, --help            show this help message and exit
  -i ISEED, --iseed ISEED
                        Random seed number (default: 21)
  -e ECM, --ecm ECM     Central Mass Energy sqrt(s). (default: None)
  --param_card PARAM_CARD
                        Instead of directly using the parameter set from run
                        number/parameters, import a MG5 parameter card. Only
                        used when generating event (default: ) (Suggested to be auto)
  -p [sim_opt [sim_opt ...]], --parton_level [sim_opt [sim_opt ...]]
                        Parton-level simulation. Powered by MG5_aMC@NLOv2.6.7
                        (default: None)
  -pv [sim_opt [sim_opt ...]], --parton_level_validation [sim_opt [sim_opt ...]]
                        Produce a ntuple that has all the neccessary parton-
                        level information inside. The Matrix Element will be
                        calculated as well. If <FFMT> is given, then the file
                        matching will be done and stored into the ntuples.
                        (default: None)
  -t [sim_opt [sim_opt ...]], --truth_level [sim_opt [sim_opt ...]]
                        Truth-level simulation. Powered by Pythia6 (default:
                        None)
  -tv [sim_opt [sim_opt ...]], --truth_level_validation [sim_opt [sim_opt ...]]
                        Produce a ntuple that has all the neccessary truth-
                        level information. RWGT will be done at the procedure.
                        (default: None)
  -c CLUSTER, --cluster CLUSTER
                        Batch system to use. (default: condor)
  --ffmt FFMT           File format that will be produced or matched (default:
                        DAOD_TRUTH1)
  --model {2HDM,2HDM+a}
                        Model to run. (default: None)
  -s SYSTEMATICS, --systematics SYSTEMATICS
                        Caculate systematics uncertainty (default: False)
```

MC Generation
-------------
LHE/DAOD_TRUTH1 Production. __You may also need this for reweighting.__
1.  For example,
    ```bash
    ./main.py parameters A 500 0.4 SI "[:1000000:10000]" 100 0 --model 2HDM+a -e 13000 -p (or -t or -pv or -tv) --param_card auto
    ```
    *   The `parameters` is used to control the model parameter, following with mediator, mass of heavy Higgs, tan\beta, signal type, slicer, mass of a, sin\theta in order.
    *   The slicer is used to control the sample size and number of events per sub-LHE file. `[:1000000:10000]` means one million events are produced and ten thousand events are stored in each sub-LHE file.
    *   With `-p`, LHE files of ME-level events are produced. With `-s`, systematics uncertainty will be considered. Add `--param_card [File-Path]` if you want to specify a parameter card.  
        with `-pv`, flat ntuples of ME-level events with reweighting-related information included are produced.  
        with `-t`, DAOD_TRUTH1 files of events after showering are produced.  
        with `-tv`, flat ntuples of events after showering with reweighting information (and matching information if any) included are produced.
    
2.   Please remember the slicing information and use the same throughout the whole event generation/reweighting procedure.
